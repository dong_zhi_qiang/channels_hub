<?php
namespace app\common;

class ChildWorkerController
{

    protected $_worker = null;

    public function forkBy(Worker $worker,$success_callback = null)
    {
        $worker->workerFork($this->_worker,$success_callback);
    }
}
