<?php


return [
    'channel_server_port' => 2206,
    'channel_http_port' => 8080,
    'channel_https_port' => 8083,
    'client_service_port' => 7000,
    'web_admin_port' => 8010,
    
];